#include<arduinoPlatform.h>
#include<tasks.h>
#include<interrupts.h>
#include<stdio.h>
#include<serial.h>
#include<pwm.h>

extern serial Serial;

//=========================================================

#define led_pin 26
#define btn_pin 4
#define sw_pin 2

#define inter_code 1

#define init_blink_period 1000
#define proizvoljan_period 100

short br = 1;
int taskBlink_id,
    old_btnState,
    freq = init_blink_period;



void increment_br(){
    (br < 10) ? (++br) : (br = 1);
}

void set_freq(){
    freq = init_blink_period / br;
    setTaskPeriod(taskBlink_id, freq);
}

void task_blink(int id, void* ptr){
     digitalWrite(led_pin, !digitalRead(led_pin));
}


void task_push(int id, void* ptr){
    int new_btnState = digitalRead(btn_pin);

    //push
    if(!old_btnState && new_btnState){
        Serial.print("Period blinkanja je: ");
        Serial.println(freq);
    } else
    //release
    if(!new_btnState && old_btnState){
        set_freq();
    }

    old_btnState = new_btnState;
}

void interrupt_down();

void interrupt_up(){
    increment_br();

    detachInterrupt(inter_code);
    attachInterrupt(inter_code, interrupt_down, FALLING);
}

void interrupt_down(){
    increment_br();

    detachInterrupt(inter_code);
    attachInterrupt(inter_code, interrupt_up, RISING);

}

void task_monitor(int id, void* ptr){
    if(Serial.available()){
        char input = Serial.read();
        input -= '0';
        if(input >= 0 && input < 10){
            br = ++input;
            set_freq();
        }
    }
}

void setup()
{
    pinMode(led_pin, OUTPUT);
    digitalWrite(led_pin, LOW);
    old_btnState = digitalRead(btn_pin);


    taskBlink_id =  createTask(task_blink, init_blink_period, TASK_ENABLE, NULL );
                    createTask(task_push, proizvoljan_period, TASK_ENABLE, NULL);
                    createTask(task_monitor, proizvoljan_period, TASK_ENABLE, NULL);

    (!digitalRead(sw_pin)) ? attachInterrupt(inter_code, interrupt_up, RISING) : attachInterrupt(inter_code, interrupt_down, FALLING);


    Serial.begin(9600);
}


void loop()
{

}







