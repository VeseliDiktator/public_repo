#include<arduinoPlatform.h>
#include<tasks.h>
#include<interrupts.h>
#include<stdio.h>
#include<serial.h>
#include<pwm.h>

extern serial Serial;

//=========================================================


#define period1 40
#define period2 50


#define led_pin 26
#define led_num 8

#define btn1_pin 4
#define btn2_pin 34
#define btn4_pin 37

#define anal_pin A0

int x1;
int analog_out,
    btn1_old_state,
    btn2_old_state,
    btn4_old_state,
    time_on;


int fun1(){
    int analog_in = analogRead(anal_pin);
    analog_out = map(analog_in, 0, 1023, 0, 8);

    for(int i = led_pin; i < led_pin + led_num; i++)
        digitalWrite(i, LOW);

    if(analog_out)
    for(int i = led_pin; i < analog_out + led_pin; i++)
        digitalWrite(i, HIGH);

    return analog_out;
}

void task1(int id, void* ptr){
    int broj_ledica = fun1();

    int btn1_new_state = digitalRead(btn1_pin),
        btn2_new_state = digitalRead(btn2_pin);

    if(!btn1_old_state && btn1_new_state)
        x1 = 0;
    if(!btn2_old_state && btn2_new_state)
        x1 += broj_ledica;

    btn1_old_state = btn1_new_state;
    btn2_old_state = btn2_new_state;
}

void interruptF(){
    Serial.println(x1);
}

void task2(int id, void* ptr){
    int btn4_new_state = digitalRead(btn4_pin);

    if(!btn4_old_state && btn4_new_state)
        time_on = millis();
    else
    if(!btn4_new_state && btn4_old_state)
        if(millis() - time_on > 5000)
            executeSoftReset(0);
            /*
            Za rad na plocici reset glasi:
             executeSoftReset(RUN_SKETCH_ON_BOOT);
            */

    btn4_old_state = btn4_new_state;
}




void setup()
{
    for(int i = led_pin; i < led_pin + led_num; i++){
        pinMode(i, OUTPUT);
        digitalWrite(i, LOW);
    }


    btn1_old_state = digitalRead(btn1_pin);
    btn2_old_state = digitalRead(btn2_pin);
    btn4_old_state = digitalRead(btn4_pin);

    attachInterrupt(1, interruptF,RISING);

    createTask(task1, period1, TASK_ENABLE, NULL);
    createTask(task2, period2, TASK_ENABLE, NULL);

    Serial.begin(9600);

    Serial.println("Ime Prezime PRXX/YYYY");
       x1 = 0;


}


void loop()
{

}







